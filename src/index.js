const mysql = require('mysql2');
const morgan = require('morgan');
const express = require('express');
const myConnection = require('express-myconnection');

const app = express();

// importing routes
const customerRoutes = require('./routes/customer');

// settings
app.set('port', process.env.PORT || 3000);

const conn = myConnection( mysql, {
        host: 'localhost',
        user: 'root',
        password: '1526',
        port: 3306,
        database: 'demo',
    },
    'single'
);

// middleware
app.use(morgan('dev'));
app.use(conn);
app.use(express.json({ limit: '10kb' }));
app.use(express.urlencoded({ extended: true }));

// routes
app.use('/', customerRoutes);

// starting the server
app.listen(app.get('port'), () => {
    console.log(`server on port ${app.get('port')}`);
});
