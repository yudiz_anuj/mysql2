const router = require('express').Router();

const customerController = require('../controllers/customerController');

router.get('/', customerController.list);
router.get('/pick/:id', customerController.pick);

router.post('/add', customerController.save);
router.patch('/update/:id', customerController.update);
router.delete('/delete/:id', customerController.delete);

module.exports = router;
